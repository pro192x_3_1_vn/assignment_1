package core.util;

public enum Century {
    TK_20(1900, 1999, "01"),
    TK_21(2000, 2099, "23"),
    TK_22(2100, 2199, "45"),
    TK_23(2200, 2299, "67"),
    TK_24(2300, 2399, "89"),
    ;
    private int from;
    private int to;
    private String gender;

    Century(int from, int to, String gender) {
        this.from = from;
        this.to = to;
        this.gender = gender;
    }

    public int from() {
        return from;
    }

    public int to() {
        return to;
    }

    public String gender() {
        return gender;
    }

    public static Century findByGender(String gender) {
        for (Century item : Century.values()) {
            if (item.gender.substring(0, 1).equals(gender) ||
                item.gender.substring(1).equals(gender)) {
                return item;
            }
        }
        return null;
    }

}
