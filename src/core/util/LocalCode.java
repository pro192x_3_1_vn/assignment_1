package core.util;

public enum LocalCode {
    HA_NOI("Hà Nội", "001"),
    HA_GIANG("Hà Giang", "002"),
    CAO_BANG("Cao Bằng", "004"),
    BAC_KAN("Bắc Kạn", "006"),
    TUYEN_QUANG("Tuyên Quang", "008"),
    LAO_CAI("Lào Cai", "010"),
    DIEN_BIEN("Điện Biên", "011"),
    LAI_CHAU("Lai Châu", "012"),
    SON_LA("Sơn La", "014"),
    YEN_BAI("Yên Bái", "015"),
    HOA_BINH("Hòa Bình", "017"),
    THAI_NGUYEN("Thái Nguyên", "019"),
    LANG_SON("Lạng Sơn", "020"),
    QUANG_NINH("Quảng Ninh", "022"),
    BAC_GIANG("Bắc Giang", "024"),
    PHU_THO("Phú Thọ", "025"),
    VINH_PHUC("Vĩnh Phúc", "026"),
    BAC_NINH("Bắc Ninh", "027"),
    HAI_DUONG("Hải Dương", "030"),
    HAI_PHONG("Hải Phòng", "031"),
    HUNG_YEN("Hưng Yên", "033"),
    THAI_BINH("Thái Bình", "034"),
    HA_NAM("Hà Nam", "035"),
    NAM_DINH("Nam Định", "036"),
    NINH_BINH("Ninh Bình", "037"),
    THANH_HOA("Thanh Hóa", "038"),
    NGHE_AN("Nghệ An", "040"),
    HA_TINH("Hà Tĩnh", "042"),
    QUANG_BINH("Quảng Bình", "044"),
    QUANG_TRI("Quảng Trị", "045"),
    THUA_THIEN_HUE("Thừa Thiên Huế", "046"),
    DA_NANG("Đà Nẵng", "048"),
    QUANG_NAM("Quảng Nam", "049"),
    QUANG_NGAI("Quảng Ngãi", "051"),
    BINH_DINH("Bình Định", "052"),
    PHU_YEN("Phú Yên", "054"),
    KHANH_HOA("Khánh Hòa", "056"),
    NINH_THUAN("Ninh Thuận", "058"),
    BINH_THUAN("Bình Thuận", "060"),
    KON_TUM("Kon Tum", "062"),
    GIA_LAI("Gia Lai", "064"),
    DAK_LAK("Đắk Lắk", "066"),
    DAK_NONG("Đắk Nông", "067"),
    LAM_DONG("Lâm Đồng", "068"),
    BINH_PHUOC("Bình Phước", "070"),
    TAY_NINH("Tây Ninh", "072"),
    BINH_DUONG("Bình Dương", "074"),
    DONG_NAI("Đồng Nai", "075"),
    VUNG_TAU("Bà Rịa - Vũng Tàu", "077"),
    HCM("Hồ Chí Minh", "079"),
    LONG_AN("Long An", "080"),
    TIEN_GIANG("Tiền Giang", "082"),
    BEN_TRE("Bến Tre", "083"),
    TRA_VINH("Trà Vinh", "084"),
    VINH_LONG("Vĩnh Long", "086"),
    DONG_THAP("Đồng Tháp", "087"),
    AN_GIANG("An Giang", "089"),
    KIEN_GIANG("Kiên Giang", "091"),
    CAN_THO("Cần Thơ", "092"),
    HAU_GIANG("Hậu Giang", "093"),
    SON_TRANG("Sóc Trăng", "094"),
    BAC_LIEU("Bạc Liêu", "095"),
    CA_MAU("Cà Mau", "096"),
    ;

    private String local;
    private String code;

    LocalCode(String local, String code) {
        this.local = local;
        this.code = code;
    }

    public String local() {
        return local;
    }

    public String code() {
        return code;
    }

    public static LocalCode findByCode(String code) {
        for (LocalCode item : LocalCode.values()) {
            if (item.code.equals(code)) {
                return item;
            }
        }
        return null;
    }

    public static boolean existByCode(String code) {
        LocalCode localCode = findByCode(code);
        return localCode != null;
    }

}
