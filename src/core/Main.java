package core;

import core.util.Century;
import core.util.LocalCode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Main {
    static final String NUMBER_AND_LETTER = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    static final Random random = new Random();

    /**
     * Hàm thực thi chính của trương trình
     * @param args Tham số hệ thống
     */
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            showMainMenu();
            doMainMenu(reader);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Thực hiện danh sách chức năng chính
     * @param reader Lớp đọc ký tự được nhập từ bàn phím
     * @return true or false
     */
    static void doMainMenu(BufferedReader reader) {
        String choose;
        boolean isTrue = true;
        do {
            try {
                choose = reader.readLine()
                    .trim();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            switch (choose) {
                case "1": {
                    doMainFirstChoice(reader);
                    showMainMenu();
                    break;
                }
                case "0", "No": {
                    isTrue = false;
                    break;
                }
                default:
                    invalidSelection();
                    break;
            }
        } while (isTrue);
    }

    /**
     * Thực hiện lựa chọn chính đầu tiên
     * @param reader Lớp đọc ký tự được nhập từ bàn phím
     */
    private static void doMainFirstChoice(BufferedReader reader) {
        String cccd;
        if (!identify(reader)) {
            return;
        }
        System.out.print("Nhập số CCCD: ");
        do {
            try {
                cccd = reader.readLine()
                    .trim();
            } catch (IOException e) {
                throw new RuntimeException("Lỗi nhập CCCD!", e);
            }
        } while (!validateCCCD(cccd));
        showSubMenu();
        doSubMenu(reader, cccd);
    }

    static Boolean chooseIdentifyWay(BufferedReader reader) {
        String choose;
        boolean isTrue = true;
        do {
            try {
                choose = reader.readLine()
                    .trim();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            switch (choose) {
                case "1": {
                    return false;
                }
                case "2": {
                    return true;
                }
                case "0", "No": {
                    isTrue = false;
                    break;
                }
                default:
                    invalidSelection();
                    break;
            }
        } while (isTrue);
        return null;
    }

    /**
     * In danh sách chức năng xác thực.
     */
    static void showIdentifyWay() {
        System.out.print("""
            \t| 1. Xác thực đơn giản.
            \t| 2. Xác thực phức tạp.
            \t| 0. Thoát.
            Chọn phương thức xác thực:\s""");
    }

    /**
     * Xác thực mã xác minh
     * @param reader Lớp đọc ký tự được nhập từ bàn phím
     */
    static boolean identify(BufferedReader reader) {
        String inputCode;
        String identifyCode;
        showIdentifyWay();
        Boolean identifyCondition = chooseIdentifyWay(reader);
        if (identifyCondition != null) {
            if (identifyCondition) {
                identifyCode = getHardIdentifyCode();
            } else {
                identifyCode = String.valueOf(getEasyIdentifyCode());
            }
        } else {
            return false;
        }
        do {
            try {
                inputCode = reader.readLine()
                    .trim();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if (identifyCode.equals(inputCode)) {
                return true;
            }
            System.out.print("Mã xác thực không đúng. Vui lòng thử lại. ");
        } while (true);
    }

    /**
     * Thực hiện danh sách chức năng phụ
     * @param reader Lớp đọc ký tự được nhập từ bàn phím
     * @param cccd   Số CCCD
     */
    static void doSubMenu(BufferedReader reader, String cccd) {
        String choose = "0";
        boolean isTrue = true;
        do {
            try {
                choose = reader.readLine()
                    .trim();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            switch (choose) {
                case "1": {
                    getDobAndGender(cccd);
                    showSubMenu();
                    break;
                }
                case "2": {
                    getBirthplace(cccd);
                    showSubMenu();
                    break;
                }
                case "3": {
                    getRandomNumberFromCCCD(cccd);
                    showSubMenu();
                    break;
                }
                case "0", "No": {
                    isTrue = false;
                    break;
                }
                default:
                    invalidSelection();
                    break;
            }
        } while (isTrue);
    }

    /**
     * In danh sách chức năng chính
     */
    static void showMainMenu() {
        System.out.print("""
            \n+--------------+----------------------------+
            | NGÂN HÀNG SỐ |      XM02181@v1.0.0        |
            +--------------+----------------------------+
            | 1. Nhập CCCD |                            |
            | 0. Thoát     |                            |
            +--------------+----------------------------+
            Chọn chức năng:\s""");
    }

    /**
     * In danh sách chức năng phụ
     */
    static void showSubMenu() {
        System.out.print("""
            \t| 1. Kiểm tra nơi sinh.
            \t| 2. Kiểm tra năm sinh, giới tính.
            \t| 3. Kiểm tra số ngẫu nhiên.
            \t| 0. Thoát.
            Chọn chức năng:\s""");
    }

    /**
     * Lấy mã xác thực đơn giản
     * @return Mã xác thực
     */
    static int getEasyIdentifyCode() {
        int code = random.nextInt(900) + 100;
        System.out.printf("Mã xác thực: %d%nNhập mã xác thực: ", code);
        return code;
    }

    /**
     * Lấy mã xác minh phức tạp
     * @return mã xác minh
     */
    static String getHardIdentifyCode() {
        int length = 6;
        StringBuilder stringBuilder = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(NUMBER_AND_LETTER.length());
            stringBuilder.append(NUMBER_AND_LETTER.charAt(index));
        }
        String code = stringBuilder.toString();
        System.out.printf("Nhập mã xác thực: %s%n", code);
        return code;
    }

    /**
     * Xác nhận giới tính từ số lấy ra từ CCCD
     * @param num số lấy từ CCCD
     * @return Nam hoặc Nữ
     */
    static String isMale(int num) {
        Set<Integer> male = new HashSet<>(Arrays.asList(0, 2, 4, 6, 8));
        if (male.contains(num)) {
            return "Nam";
        } else {
            return "Nữ";
        }
    }

    /**
     * Xác nhận lại số CCCD
     * @param cccd Số CCCD
     * @return true or false
     */
    static boolean validateCCCD(String cccd) {
        if (cccd.length() != 12 || !cccd.matches("\\d+")) {
            invalidCCCD();
            return false;
        }
        String province = cccd.substring(0, 3);
        if (!LocalCode.existByCode(province)) {
            invalidCCCD();
            return false;
        }
        int gender = Integer.parseInt(cccd.substring(3, 4));
        if (gender > 9 || gender < 0) {
            invalidCCCD();
            return false;
        }
        return true;
    }

    /**
     * In cảnh báo khi số CCCD không hợp lệ
     */
    static void invalidCCCD() {
        System.out.println("Số CCCD không hợp lệ.\nVui lòng nhập lại:");
    }

    /**
     * In cảnh báo khi nhập ký tự không có trong bảng lựa chọn
     */
    static void invalidSelection() {
        System.out.print("Lựa chọn không hợp lệ!\nVui lòng nhập lại hoặc 'No' để thoát: ");
    }

    /**
     * Lấy 6 số tự nhiên từ CCCD
     * @param cccd Số CCCD
     */
    static void getRandomNumberFromCCCD(String cccd) {
        System.out.printf("Số ngẫu nhiên: %s%n", cccd.substring(6));
    }

    /**
     * Lấy ngày sinh từ CCCD
     * @param cccd Số CCCD
     */
    static void getBirthplace(String cccd) {
        String province = cccd.substring(0, 3);
        try {
            String birthplace = LocalCode.findByCode(province)
                .local();
            System.out.printf("Nơi sinh: %s%n", birthplace);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lấy số ngày sinh và giới tính từ CCCD
     * @param cccd Số CCCD
     */
    static void getDobAndGender(String cccd) {
        try {
            String gender = cccd.substring(3, 4);
            int century = Century.findByGender(gender)
                .from();
            int year = Integer.parseInt(cccd.substring(4, 6)) + century;
            System.out.printf("Giới tính: %s | Năm sinh: %d%n", isMale(Integer.parseInt(gender)), year);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
